import java.util.Scanner;
public class XOgame {
    char[][] board = new char[3][3];
    char x = ('x');
    char o = ('o');
    // Character.toLowerCase
    Scanner input = new Scanner(System.in);



    public XOgame() {   //start game const
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '_';
            }
        }
    }

    public void DisplayBoard() {
        for (int i = 0; i < board.length; ++i) {
            for(int j = 0; j < board[i].length; ++j) {
                System.out.print(board[i][j]);
            }
            System.out.print(" \n");
        }
    }

    public void setBoard(char[][] board) {
        this.board = board;
    }

    private  void setX(int row,int column){
        if (board[row-1][column-1] == ('_')) {
            board[row-1][column-1] = x;
        } else {
            System.out.println("There are already letters in the blank space.\nPlease enter again!:");
            row = input.nextInt();
            column = input.nextInt();
            setX(row, column);
        }
        
    }

    private void setO(int row,int column){   
        if (board[row-1][column-1] == ('_')) {
            board[row-1][column-1] = o;
        } else {
            System.out.println("There are already letters in the blank space.\nPlease enter again!:");
            row = input.nextInt();
            column = input.nextInt();
            setO(row, column);
        }
        
    }

    private boolean Xcondition(){
        return (board[0][0] == 'x' && board[0][1] == 'x' && board[0][2] == 'x') || 
        (board[1][0] == 'x' && board[1][1] == 'x' && board[1][2] == 'x') ||
        (board[2][0] == 'x' && board[2][1] == 'x' && board[2][2] == 'x') ||
        (board[0][0] == 'x' && board[1][0] == 'x' && board[2][0] == 'x') ||
        (board[0][1] == 'x' && board[1][1] == 'x' && board[2][1] == 'x') ||
        (board[0][2] == 'x' && board[1][2] == 'x' && board[2][2] == 'x') ||
        (board[0][0] == 'x' && board[1][1] == 'x' && board[2][2] == 'x') ||
        (board[0][2] == 'x' && board[1][1] == 'x' && board[2][0] == 'x');
    }
    
    private boolean Ocondition(){
        return (board[0][0] == 'o' && board[0][1] == 'o' && board[0][2] == 'o') || 
        (board[1][0] == 'o' && board[1][1] == 'o' && board[1][2] == 'o') ||
        (board[2][0] == 'o' && board[2][1] == 'o' && board[2][2] == 'o') ||
        (board[0][0] == 'o' && board[1][0] == 'o' && board[2][0] == 'o') ||
        (board[0][1] == 'o' && board[1][1] == 'o' && board[2][1] == 'o') ||
        (board[0][2] == 'o' && board[1][2] == 'o' && board[2][2] == 'o') ||
        (board[0][0] == 'o' && board[1][1] == 'o' && board[2][2] == 'o') ||
        (board[0][2] == 'o' && board[1][1] == 'o' && board[2][0] == 'o');
    }

    // private boolean Drawcondition(){
    //     for (int i = 0; i < 3; i++) {
    //         for (int j = 0; j < 3; j++) {
    //             if (board[i][j] != '_'){
    //                 return true;
    //             }
    //         }
    //     }
    // }

    public void play(){
        System.out.println("Welcome to XO games");
        // while (true) {
        //     DisplayBoard();
        //     setX(rows, columns);
        //     if (Xcondition()) {
        //         System.out.println("Player X wins");
        //         break;
        //     }

        //     DisplayBoard();
        //     setO(rows, columns);
        //     if (Ocondition()) {
        //         System.out.println("Player O wins");
        //         break;
        //     }
        // }
        while(true){
            try{
                System.out.println("");
                DisplayBoard();
                System.out.println("Player X's turn\nEnter row and column: ");
                int rowsX = input.nextInt();
                int columnsX = input.nextInt();
                setX(rowsX, columnsX);
                if (Xcondition()) {
                    DisplayBoard();
                    System.out.println("Player X wins!");
                    break;
                }
            } catch (Exception e) {
                System.out.println("Invalid input or position.Try again.");
                break;
            }
        
            try{
                System.out.println("");
                DisplayBoard();
                System.out.println("Player O's turn\nEnter row and column: ");
                int rowsO = input.nextInt();
                int columnsO = input.nextInt();
                setO(rowsO, columnsO);
                if (Ocondition()) {
                    DisplayBoard();
                    System.out.println("Player O wins!");
                    break;
                }
            } catch (Exception e) {
                System.out.println("Invalid input or position. Try again.");
                break;
            }
        }
        
        
    }




}
